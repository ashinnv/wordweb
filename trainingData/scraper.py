#!/usr/bin/python3

import argparse
import wikipediaapi
import re

def get_plain_text(article_title):
    wiki_wiki = wikipediaapi.Wikipedia('Mozilla/5.0 (X11; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0')  # Specify the language code, e.g., 'en' for English

    page = wiki_wiki.page(article_title)
    
    if not page.exists():
        return "Article not found."
    
    wiki_text = page.text
    plain_text = re.sub(r'\[\d+\]', '', wiki_text)  # Remove reference numbers
    
    return plain_text

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fetch and display plain text from a Wikipedia article.')
    parser.add_argument('article_title', type=str, help='Title of the Wikipedia article')
    
    args = parser.parse_args()
    article_title = args.article_title
    
    plain_text = get_plain_text(article_title)
    print(plain_text)

