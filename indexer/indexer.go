package main

import(
	"os"
	"fmt"
	"flag"
	"sync"
	"time"
	"strings"
	"regexp"
	"io/ioutil"
	"encoding/gob"
	"database/sql"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"

)

//=================================

type Index struct{
	Index uint
	Word  string
}

func (idx Index)GetIndex() uint {
	return idx.Index
}

func (idx Index)GetWord() string {
	return idx.Word
}

//=================================

func main(){

	datLoc := flag.String("datloc", "", "location of training data")
	uname  := flag.String("uname",  "root", "root username for sql")
	passwd := flag.String("passwd", "",     "password for sql")
	flag.Parse()

	fileChan := make(chan string)
	splitChan := make(chan string)
	toMap := make(chan string)
	dbChan := make(chan []Index)

	go processFiles(*datLoc, fileChan)
	go splitWords(fileChan, splitChan)
	go cleanWords(splitChan, toMap)
	go manageMap(toMap, dbChan)
	go manageSql(*uname, *passwd, dbChan)

	var wg sync.WaitGroup
	wg.Add(100)


	wg.Wait()
	fmt.Println("Done waiting.")
}


func manageSql(user string, password string, dbChan chan []Index) {
	//waitval := 5 //How long to wait between checks of master slice length	

	//Looping so that if we lose the connection, we cn retry. (Not yet implemented)
	for {
		db, err := sql.Open("mysql",
			fmt.Sprintf("%s:%s@tcp(127.0.0.1:3306)/capperCloud",
				user,
				password))

		if err != nil {
			panic(err.Error())
		}

		defer db.Close()

		//for npt := range input{
		//for word,index := range npt{
		for inSlice := range dbChan{

			for _,ob := range inSlice{
			insert, err := db.Query("INSERT INTO word_indexes VALUES (?, ?)", ob.Word, ob.Index)
			if err != nil {
				panic(err.Error())
			}
			defer insert.Close()

			go storeMaster(inSlice)
		}

		}
	}
}

//put what we've put into database into a second cache file for this program's exclusive use
func storeMaster(input []Index){
	if fileHandle, fileErr := os.OpenFile("gobStore", os.O_RDWR|os.O_CREATE, 0755); fileErr != nil{
		panic("FILE OPEN ERROR: "+fileErr.Error())
	}else{
		enc := gob.NewEncoder(fileHandle)
		if encerr := enc.Encode(&input); encerr != nil{
			panic("COULDN'T DECODE: "+encerr.Error())
		}
	}

}

//pull the stuff from the cache file
func loadMaster() []Index {

	var finNdx []Index
	if fileHandle, fileErr := os.OpenFile("gobStore", os.O_RDWR|os.O_CREATE, 0755); fileErr != nil{
		//panic("FILE OPEN ERROR: "+fileErr.Error())
		fmt.Println("Error: ", fileErr)
		return []Index{}
	}else{
		dec := gob.NewDecoder(fileHandle)
		if decerr := dec.Decode(&finNdx); decerr != nil{
			//panic("COULDN'T DECODE: "+decerr.Error())
			fmt.Println("Error: ", fileErr)
			return []Index{}
		}else{
			return finNdx
		}
	}

}


//We're just using the map for deduplication. Bools are empty and useless
func manageMap(input chan string, toDb chan []Index){

	finSlice := loadMaster()

	storeMap := make(map[string]bool)
	waitVal  := 5

	//Find new strings and add them to the final slice
	go func(){
		for{
			if len(storeMap) == len(finSlice){
				time.Sleep(time.Duration(waitVal) * time.Second)
				continue
			}

			new := getNew(storeMap, finSlice)
			finSlice = append(finSlice, new...)
			toDb <- finSlice
		}
	}()

	for word := range input{
		storeMap[word] = true
	}
}

func getNew(storeMap map[string]bool, finSlice []Index) []Index {
	storeSlice := []Index{}
	var sliceMtx sync.Mutex
	var wg sync.WaitGroup

	for key,_ := range storeMap{
		wg.Add(1)
		go func(keystring string, finSlc []Index){
			for _,word := range finSlc{
				if word.Word == keystring{
					return
				}
			}
			sliceMtx.Lock()
			storeSlice = append(storeSlice, Index{0x0,keystring})
			sliceMtx.Unlock()
			wg.Done()
		}(key, finSlice)
	}

	wg.Wait()
	return storeSlice
}

func cleanWord(input string) string{
	// Create a regular expression to match non-alphanumeric characters
	regex := regexp.MustCompile("[^a-zA-Z0-9]+")
	
	// Replace non-alphanumeric characters with an empty string
	result := regex.ReplaceAllString(input, "")
	
	return result
	
}

func cleanWords(input chan string, output chan string){
	for cleanMe := range input{
		go func(){
			output <- cleanWord(cleanMe)
		}()
	}
}

func splitWords(input chan string, output chan string){
	for doc := range input{
		go func(){
			for _,wrd := range strings.Split(doc, " "){
				output <- wrd
			}
		}()
	}
}

func processFiles(fLoc string, out chan string){
	if filenames, fnErr := getFilenames(fLoc); fnErr != nil{
		panic("Error with processFiles function: "+fnErr.Error()+" "+fLoc)
	}else{
		for _,fname := range filenames{
			readFile(fname, out)
		}
	}

}

func readFile(name string, output chan string){

	if fdat, fileErr := ioutil.ReadFile(name); fileErr != nil {
		panic("Couldn't read file: "+fileErr.Error())
	}else{
		output <- string(fdat)
	}
}

func getFilenames(targ string) ([]string, error) {
	if fnames, fnErr := filepath.Glob(targ+"/*.txt"); fnErr != nil{
		return []string{},fnErr
	}else{
		return fnames,nil
	}
}