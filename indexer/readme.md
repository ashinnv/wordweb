# Indexer

## Description
Indexer is a standalone utility that is pointed at the training data and creates a word index database by breaking up the training data into independent words. This is done for performance reasons. 

## Pipeline
Indexer is a pipelined application. As of the time of this writing, Indexer has five stages run as goroutines that communicate by channels. The first manages the training data and makes sure all training data has been read through and consumed. The second stage splits the words by spaces. The third cleans all non-alphanumeric characters out of a word. The fourth deduplicates the data and the fifth writes that data to the database for other applications and saves the word data to a cache file.

